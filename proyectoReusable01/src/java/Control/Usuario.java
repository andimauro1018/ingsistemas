    
package Control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.faces.application.FacesMessage;

@ManagedBean
@RequestScoped
@SessionScoped
public class Usuario {

    private int id;
    private String name,lastname,address,phone,password,email;
    private int tusu,sector;
    private String valEmail, valPassword;
    
    Connection connection;
    String sql_String;
    ArrayList usersList;
    private Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

    
    public Connection getConnection(){
        
        try{
            
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/reusable","root","");
        }catch(Exception e){
            System.out.println(e);
        }
        
        return connection;
    }
    
   
    public String saveData(){
        
        int result = 0;
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO `usuarios`(`Nombres`, `Apellidos`, `correo`, `clave`, `Direccion`, "
                                                             + "`TelefonoMovil`, `Tipo_id`, `Sector_id`, `Activo`) VALUES (?,?,?,?,?,?,?,?,'SI')");
            stmt.setString(1,name);
            stmt.setString(2,lastname);
            stmt.setString(3,email);
            stmt.setString(4,password);
            stmt.setString(5,address);
            stmt.setString(6,phone);
            stmt.setInt(7,tusu);
            stmt.setInt(8,sector);
            result = stmt.executeUpdate();
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        if(result != 0)
            return "index.xhtml?faces-redirect=true";
        else return "registro.xhtml?faces-redirect=true";
    }
    
    public String registrarAdmin(){
        
        int result = 0;
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO `usuarios`(`Nombres`, `Apellidos`, `correo`, `clave`, `Direccion`, "
                                                             + "`TelefonoMovil`, `Tipo_id`, `Sector_id`, `Activo`) VALUES (?,?,?,?,?,?,?,?,'SI')");
            stmt.setString(1,name);
            stmt.setString(2,lastname);
            stmt.setString(3,email);
            stmt.setString(4,password);
            stmt.setString(5,address);
            stmt.setString(6,phone);
            stmt.setInt(7,tusu);
            stmt.setInt(8,sector);
            result = stmt.executeUpdate();
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        if(result != 0)
            return "PrinciapalAdmin.xhtml?faces-redirect=true";
        else return "PrinciapalAdmin.xhtml?faces-redirect=true";
    }
    
   public void dbBuscador(String correO){
        try {
            connection = getConnection();
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from usuarios where correo = '"+ correO +"' ");
            
            while(rs.next()){
                id = rs.getInt("Usuario_id");
                name = rs.getString("Nombres");
                lastname =rs.getString("Apellidos");
                valEmail = rs.getString("correo");
                valPassword = rs.getString("clave");
                address = rs.getString("Direccion");
                phone = rs.getString("TelefonoMovil");
                tusu = rs.getInt("Tipo_id");
                sector = rs.getInt("Sector_id");
                
            }   
            
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
    
    public String actualiza(Usuario u){
       
       try{
           connection = getConnection();
            PreparedStatement stmt=connection.prepareStatement("UPDATE usuarios SET correo =?,clave =?,Direccion =?, Sector_id =? WHERE usuarios.Usuario_id =?; ");
            stmt.setString(1, u.getEmail());
            stmt.setString(2, u.getPassword());
            stmt.setString(3, u.getAddress());
            stmt.setInt(4, u.getSector());
            stmt.setInt(5, u.getId());
            stmt.executeUpdate();
            connection.close();
       
       }
       catch( Exception e){
           e.printStackTrace();
           System.out.print(e);
       }
       int aux = u.getTusu();
       if (aux == 1){
           return "/PrincipalVerde.xhtml?faces-redirect=true";
       }else{
           return "/PrincipalRecolector.xhtml?faces-redirect=true";
       }     
   }

    public String validarLogin(){
        dbBuscador(email);
        if(email.equalsIgnoreCase(valEmail)){
            if(password.equals(valPassword) && tusu == 1){
                
                Usuario usu = new Usuario(id,name,lastname,valEmail,valPassword,address,phone,tusu,sector);
                HttpSession hs = Util.getSession();
                hs.setAttribute("usuario", usu);
                return "verde";
                
            }else if(password.equals(valPassword) && tusu == 2){
                
                Usuario usu = new Usuario(id,name,lastname,valEmail,valPassword,address,phone,tusu,sector);
                HttpSession hs = Util.getSession();
                hs.setAttribute("usuario", usu);
                return "recolector";
                
            }else if(password.equals(valPassword) && tusu == 3){
                
                Usuario usu = new Usuario();
                HttpSession hs = Util.getSession();
                hs.setAttribute("usuario", usu);
                return "admin";
                
            }else{
                FacesMessage fm = new FacesMessage("Login Error", "ERROR MSG");
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, fm);
                return "index.xhtml?faces-redirect=true";
            }
        }else{
            FacesMessage fm = new FacesMessage("Login Error", "ERROR MSG");
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, fm);
                return "index.xhtml?faces-redirect=true";
        }
    }
    
    public String cerrarSesion(){
        HttpSession hs = Util.getSession();
        hs.invalidate();
        return "index.xhtml?faces-redirect=true";
    }
    
    
    
    public void cancelar(int idu,int rcid){
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE programados SET Estado_id = '3' WHERE programados.Programacion_id= '" + rcid + "' AND programados.UsuarioVerde_id =" + idu);
            stmt.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void desactivarU(int idu){
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE `usuarios` SET `Activo` = 'NO' WHERE `usuarios`.`Usuario_id` = "+ idu +"; ");
            stmt.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void activarU(int idu){
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE `usuarios` SET `Activo` = 'SI' WHERE `usuarios`.`Usuario_id` = "+ idu +"; ");
            stmt.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public String getValEmail() {
        return valEmail;
    }

    public void setValEmail(String valEmail) {
        this.valEmail = valEmail;
    }

    public String getValPassword() {
        return valPassword;
    }

    public void setValPassword(String valPassword) {
        this.valPassword = valPassword;
    }
    
    
    
    

    public Usuario(int id,String name, String lastname, String email, String password, String address, String phone, int tusu, int sector) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.phone = phone;
        this.password = password;
        this.email = email;
        this.tusu = tusu;
        this.sector = sector;
    }
    
    public Usuario() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTusu() {
        return tusu;
    }

    public void setTusu(int tusu) {
        this.tusu = tusu;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}

