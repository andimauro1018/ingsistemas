
package Modelo;

import javax.inject.Named;
import javax.enterprise.context.Dependent;

@Named(value = "mensajeTip")
@Dependent
public class mensajeTip {

    private String mensaje = "";
   
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public mensajeTip() {
    }
    
    
    public String menstip(){
        int cons = (int)(Math.random() * (5 - 1 + 1) + 1);
        
        switch(cons){
            case 1:
                mensaje = "Generamos 21,5 millones de toneladas de residuos alimenticios cada año."
                        + "Si se compostasen estos residuos, se reduciría la misma cantidad de gases"
                        + "de efecto invernadero que retirar 2 millones de automóviles de la carretera.";
                break;
            case 2:
                mensaje = "Reciclando una lata de aluminio ayudas a ahorrar energía suficiente para hacer"
                        + "funcionar una televisión durante unas 3 horas.";
                break;
            case 3:
                mensaje = "Los envases metálicos se pueden reciclar indefinidamente sin perder calidad evitando"
                        + "extraer nuevos materiales.";
                break;
            case 4:
                mensaje = "Para fabricar 1.000 Kg de papel de buena calidad se necesitan unos 3.300 Kg de madera."
                        + "Reciclar papel y cartón es esencial para economizar energía, evitar la contaminación y"
                        + "el desperdicio del agua y salvar los bosques";
                break;
            case 5:
                mensaje = "El vidrio es 100% reciclable Puede reciclarse infinitamente, jamás pierde sus propiedades."
                        + "El reciclaje de vidrio ahorra 20% y 50% de la contaminación atmosférica y de las aguas respectivamente."
                        + "Globalmente se reciclan 80,000 toneladas de vidrio y se arrojan 920,000 toneladas al ambiente.";
                break;
        }
        
        return mensaje;
    }
    
}
