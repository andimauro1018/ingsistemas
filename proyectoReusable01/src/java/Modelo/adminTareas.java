
package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.Dependent;


@Named(value = "adminTareas")
@Dependent
public class adminTareas {


    
    Connection connection;
    
    // lista de usuarios   
    private int idu;
    private String nombre, apellido, correo,tipoU ,estado;
    ArrayList usuList;
    // 
    
    // lista de premios
    private int id_premiacion;
    private String nomUsuv;
    private String premio;
    ArrayList premioList;
    
    public adminTareas() {
    }
    
    public Connection getConnection(){
        
        try{
            
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/reusable","root","");
        }catch(Exception e){
            System.out.println(e);
        }
        
        return connection;
    }
    
    
    public ArrayList usuarioList(){
        try{
            usuList = new ArrayList();
            connection = getConnection();
            Statement stmt=getConnection().createStatement();
            ResultSet rs=stmt.executeQuery("SELECT usuarios.Usuario_id,usuarios.Nombres,usuarios.Apellidos,usuarios.correo,tipousuario.TipoUsuario,usuarios.Activo \n" +
                                            "FROM usuarios INNER JOIN tipousuario\n" +
                                            "ON tipousuario.Tipo_id = usuarios.Tipo_id\n" +
                                            "WHERE usuarios.Tipo_id != 3");
            
            while(rs.next()){
                adminTareas pro = new adminTareas();
                pro.setIdu(rs.getInt("Usuario_id"));
                pro.setNombre(rs.getString("Nombres"));
                pro.setApellido(rs.getString("Apellidos"));
                pro.setCorreo(rs.getString("correo"));
                pro.setTipoU(rs.getString("TipoUsuario"));
                pro.setEstado(rs.getString("Activo"));
                
                usuList.add(pro);
            }
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        
        return usuList;
    }
    
    public ArrayList listaPremios(){
        try{
            premioList = new ArrayList();
            connection = getConnection();
            Statement stmt=getConnection().createStatement();
            ResultSet rs=stmt.executeQuery("SELECT premiacion.Premiacion_id, usuarios.Nombres, usuarios.Apellidos,premios.Premio\n" +
                                            "FROM premiacion LEFT JOIN usuarios ON premiacion.UsuarioVerde_id = usuarios.Usuario_id\n" +
                                                            "LEFT JOIN premios ON premiacion.Premio_id = premios.Premio_id");
  
            while(rs.next()){
                adminTareas pro = new adminTareas();
                pro.setId_premiacion(rs.getInt("Premiacion_id"));
                pro.setNomUsuv(rs.getString("Nombres") + " " + rs.getString("Apellidos"));
                pro.setPremio(rs.getString("Premio"));
                
                premioList.add(pro);
            }
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        
        return premioList;
    }

    public int getIdu() {
        return idu;
    }

    public void setIdu(int idu) {
        this.idu = idu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipoU() {
        return tipoU;
    }

    public void setTipoU(String tipoU) {
        this.tipoU = tipoU;
    }

    public int getId_premiacion() {
        return id_premiacion;
    }

    public void setId_premiacion(int id_premiacion) {
        this.id_premiacion = id_premiacion;
    }

    public String getNomUsuv() {
        return nomUsuv;
    }

    public void setNomUsuv(String nomUsuv) {
        this.nomUsuv = nomUsuv;
    }

    public String getPremio() {
        return premio;
    }

    public void setPremio(String premio) {
        this.premio = premio;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
        
    
}
