/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Control.Usuario;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Named(value = "progranacionReci")
@Dependent
public class ProgramacionReci {

    private int id_prog;
    private String usuRec;
    private String usuVer;
    private int id_usuVer;
    private String dirVer;
    private Date fecha;
    private String estado;
    Usuario usu = new Usuario();

    Connection connection;
    ArrayList progList;

    public ProgramacionReci() {
    }

    public Connection getConnection() {

        try {

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/reusable", "root", "");
        } catch (Exception e) {
            System.out.println(e);
        }

        return connection;
    }

    // Consulta del usuario verde para ver sus Solicitudes
    public ArrayList programadosListVer(int id) {
        try{
            progList = new ArrayList();
            connection = getConnection();
            Statement stmt=getConnection().createStatement();
            
            ResultSet rs=stmt.executeQuery("SELECT programados.Programacion_id, usuarios.Nombres, usuarios.Apellidos, programados.Fecha, estados.Estado  \n" +
                                            "FROM programados LEFT JOIN usuarios ON programados.UsuarioRecolector_id =  usuarios.Usuario_id \n" +
                                                             "LEFT JOIN estados ON programados.Estado_id = estados.Estado_id \n" +
                                            "WHERE programados.UsuarioVerde_id = ' " + id +" '  \n" +
                                            "ORDER BY Fecha");
            
            while(rs.next()){
                ProgramacionReci pro = new ProgramacionReci();
                pro.setId_prog(rs.getInt("Programacion_id"));
                pro.setUsuRec(rs.getString("Nombres") + " " + rs.getString("Apellidos"));
                pro.setFecha(rs.getDate("Fecha"));
                pro.setEstado(rs.getString("Estado"));
                
                progList.add(pro);
                if(pro.getUsuRec().equals("null null")){
                    pro.setUsuRec("Sin asignar");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return progList;
    }
    
    

    //consulta de usuario recolector para ver sus recolecciones programadas.
    public ArrayList programadosListrec(int id) {
        try {
            progList = new ArrayList();
            connection = getConnection();
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT programados.Programacion_id, usuarios.Nombres, usuarios.Apellidos, usuarios.Direccion, programados.Fecha FROM programados INNER JOIN usuarios ON programados.UsuarioVerde_id =  usuarios.Usuario_id WHERE programados.UsuarioRecolector_id = " + id + " order by Fecha");

            while (rs.next()) {
                ProgramacionReci pro = new ProgramacionReci();
                pro.setId_prog(rs.getInt("Programacion_id"));
                pro.setUsuVer(rs.getString("Nombres") + " " + rs.getString("Apellidos"));
                pro.setDirVer(rs.getString("Direccion"));
                pro.setFecha(rs.getDate("Fecha"));

                progList.add(pro);
            }
            connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return progList;
    }

    //consulta de usuario recolector para conocer las programaciones disponibles.
    public ArrayList listaProgrDisponible(int id) {
        try {
            progList = new ArrayList();
            connection = getConnection();
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT programados.Programacion_id, usuarios.Nombres, usuarios.Apellidos, usuarios.Direccion, programados.Fecha FROM programados INNER JOIN usuarios ON programados.UsuarioVerde_id =  usuarios.Usuario_id WHERE programados.UsuarioRecolector_id IS NULL order by Fecha");

            while (rs.next()) {
                ProgramacionReci pro = new ProgramacionReci();
                pro.setId_prog(rs.getInt("Programacion_id"));
                pro.setUsuVer(rs.getString("Nombres") + " " + rs.getString("Apellidos"));
                pro.setDirVer(rs.getString("Direccion"));
                pro.setFecha(rs.getDate("Fecha"));

                progList.add(pro);
            }
            connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return progList;
    }
    //solicitud de usuario recolector para pedir una recoleccion especifica.

    public void asignarRec(int pro, int id) {
        try {
            connection = getConnection();
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE programados SET UsuarioRecolector_id = ? WHERE Programacion_id = ?");
            stmt.setInt(1, id);
            stmt.setInt(2, pro);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    //solicitud de usuario recolector para cancelar una recoleccion especifica.
    public void canceAsignarRec(int pro) {
        try {
            connection = getConnection();
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE programados SET UsuarioRecolector_id = NULL WHERE Programacion_id = ?");
            stmt.setInt(1, pro);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }
//solicitud de usuario recolector de los usuarios verdes a calificar.

    public ArrayList listaUsuariosVer(int id) {
        try {
            progList = new ArrayList();
            connection = getConnection();
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT programados.Programacion_id, programados.UsuarioVerde_id, usuarios.Nombres, usuarios.Apellidos, programados.Fecha FROM programados INNER JOIN usuarios ON programados.UsuarioVerde_id =  usuarios.Usuario_id WHERE programados.UsuarioRecolector_id =" + id + " AND programados.Estado_id = 1 order by Fecha");

            while (rs.next()) {
                ProgramacionReci pro = new ProgramacionReci();
                pro.setId_prog(rs.getInt("Programacion_id"));
                pro.setId_usuVer(rs.getInt("UsuarioVerde_id"));
                pro.setUsuVer(rs.getString("Nombres") + " " + rs.getString("Apellidos"));
                pro.setFecha(rs.getDate("Fecha"));

                progList.add(pro);
            }
            connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return progList;
    }

    //Metodo para calificar positivamente a un usuario verde.
    public void bonificarVerde(int pro, int id) {
        int aux = 0;
        try {
            connection = getConnection();
            Statement stmt2 = getConnection().createStatement();
            ResultSet rs = stmt2.executeQuery("SELECT PuntosGanados FROM bonificaciones WHERE UsuarioVerde_id = " + id);
            while (rs.next()) {
                aux = rs.getInt("PuntosGanados");
                aux += 10;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        try {
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE bonificaciones SET PuntosGanados = " + aux + " WHERE UsuarioVerde_id = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE programados SET Estado_id = 2 WHERE Programacion_id = ?");
            stmt.setInt(1, pro);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    //Metodo para calificar negativamente a un usuario verde.
    public void penalizarVerde(int pro, int id) {
        int aux = 0;
        try {
            connection = getConnection();
            Statement stmt2 = getConnection().createStatement();
            ResultSet rs = stmt2.executeQuery("SELECT PuntosGanados FROM bonificaciones WHERE UsuarioVerde_id = " + id);
            while (rs.next()) {
                aux = rs.getInt("PuntosGanados");
                if (aux > 15){
                    aux-= 15;
                }else{
                    aux = 0;
                }              
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        try {
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE bonificaciones SET PuntosGanados = " + aux + " WHERE UsuarioVerde_id = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {
            Statement st = getConnection().createStatement();
            PreparedStatement stmt = connection.prepareStatement("UPDATE programados SET Estado_id = 2 WHERE Programacion_id = ?");
            stmt.setInt(1, pro);
            stmt.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public String evaluarTipoUsu(int id) {
        
       if (id == 1){
           return "/PrincipalVerde.xhtml?faces-redirect=true";
       }else{
           return "/PrincipalRecolector.xhtml?faces-redirect=true";
       }  
    }
    

    public int getId_prog() {
        return id_prog;
    }

    public void setId_prog(int id_prog) {
        this.id_prog = id_prog;
    }

    public String getUsuRec() {
        return usuRec;
    }

    public void setUsuRec(String usuRec) {
        this.usuRec = usuRec;
    }

    public String getUsuVer() {
        return usuVer;
    }

    public int getId_usuVer() {
        return id_usuVer;
    }

    public void setId_usuVer(int id_usuVer) {
        this.id_usuVer = id_usuVer;
    }

    public String getDirVer() {
        return dirVer;
    }

    public void setDirVer(String dirVer) {
        this.dirVer = dirVer;
    }

    public Usuario getUsu() {
        return usu;
    }

    public void setUsu(Usuario usu) {
        this.usu = usu;
    }

    public void setUsuVer(String usuVer) {
        this.usuVer = usuVer;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList getProgList() {
        return progList;
    }

    public void setProgList(ArrayList progList) {
        this.progList = progList;
    }
}
