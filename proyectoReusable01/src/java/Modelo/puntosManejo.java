/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author juan
 */
@Named(value = "puntos")
@Dependent
public class puntosManejo {

    Connection connection;

    public puntosManejo() {
    }

    public Connection getConnection() {

        try {

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/reusable", "root", "");
        } catch (Exception e) {
            System.out.println(e);
        }

        return connection;
    }

    private int categoria1 = 5;
    private int categoria2 = 140;
    private int categoria3 = 150;
    private int categoria4 = 250;
    private int categoria5 = 360;
    private String mensaje = "";
    private int puntos;

    // varibles de los premios
    private int id_premio;
    private String n_premio;
    private String descripcion;

    public int obtpuntoS(int i) {
        try {
            connection = getConnection();
            Statement stmt = getConnection().createStatement();
            ResultSet pt = stmt.executeQuery("SELECT * FROM `bonificaciones` WHERE `UsuarioVerde_id`= '" + i + "' ");

            while (pt.next()) {
                puntos = pt.getInt("PuntosGanados");
            }

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }

        return puntos;
    }

    public String canjeoPuntos(int iden, int id) {
        int aux = 0;
        try {
            connection = getConnection();
            Statement stmt2 = getConnection().createStatement();
            ResultSet rs = stmt2.executeQuery("SELECT PuntosGanados FROM bonificaciones WHERE UsuarioVerde_id = " + id);
            while (rs.next()) {
                aux = rs.getInt("PuntosGanados");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        if (aux >= iden) {
            aux -= iden;

            try {
                Statement st = getConnection().createStatement();
                PreparedStatement stmt = connection.prepareStatement("UPDATE bonificaciones SET PuntosGanados = " + aux + " WHERE UsuarioVerde_id = ?");
                stmt.setInt(1, id);
                stmt.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                System.out.println(e);
            }
            try {
                int result = 0;
                connection = getConnection();
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO premiacion(UsuarioVerde_id, Premio_id) VALUES (?,?)");
                stmt.setInt(1, id);

                switch (iden) {
                    case 5:
                        stmt.setInt(2, 1);
                        break;
                    case 140:
                        stmt.setInt(2, 2);
                        break;
                    case 150:
                        stmt.setInt(2, 3);
                        break;
                    case 250:
                        stmt.setInt(2, 4);
                        break;
                    case 360:
                        stmt.setInt(2, 5);
                        break;
                }
                result = stmt.executeUpdate();
                connection.close();
                
            } catch (Exception e) {
                System.out.println(e);
            }
        }else{
            return "/CanjeoPremio.xhtml";
        }
        return "/CanjeoPremio.xhtml";
    }

    public String redimir1(int punt, int idu) {
        int aux;

        aux = punt - categoria1;
        try {
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE bonificaciones SET bonificaciones.PuntosGanados =?"
                    + " WHERE bonificaciones.UsuarioVerde_id =? ");
            stmt.setInt(1, aux);
            stmt.setInt(2, idu);

            stmt.executeUpdate();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e);
        }

        return "/CanjeoPremio.xhtml";

    }

    public int getId_premio() {
        return id_premio;
    }

    public void setId_premio(int id_premio) {
        this.id_premio = id_premio;
    }

    public String getN_premio() {
        return n_premio;
    }

    public void setN_premio(String n_premio) {
        this.n_premio = n_premio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getCategoria1() {
        return categoria1;
    }

    public void setCategoria1(int categoria1) {
        this.categoria1 = categoria1;
    }

    public int getCategoria2() {
        return categoria2;
    }

    public void setCategoria2(int categoria2) {
        this.categoria2 = categoria2;
    }

    public int getCategoria3() {
        return categoria3;
    }

    public void setCategoria3(int categoria3) {
        this.categoria3 = categoria3;
    }

    public int getCategoria4() {
        return categoria4;
    }

    public void setCategoria4(int categoria4) {
        this.categoria4 = categoria4;
    }

    public int getCategoria5() {
        return categoria5;
    }

    public void setCategoria5(int categoria5) {
        this.categoria5 = categoria5;
    }

}
